import React, { Component } from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import indigo from '@material-ui/core/colors/indigo';

import 'typeface-roboto';

import Main from './components/main/Main';

const theme = createMuiTheme({
  palette: {
    primary: indigo
  }
});

class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Main/>
      </MuiThemeProvider>
    );
  }
}

export default App;
