import { compose } from 'recompose';
import { withStyles } from '@material-ui/core/styles/index';
import ItemComponent from './ItemComponent';

const styles = {
  card: {
    margin: 25,
    width: 400,
    height: 400,
  },
  media: {
    height: 295,
  },
  actions: {
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center'
  }
};

const enhance = compose(
  withStyles(styles)
);

const Item = enhance(ItemComponent);

export default Item;