import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography/Typography';
import Card from '@material-ui/core/Card/Card';
import CardActionArea from '@material-ui/core/CardActionArea/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia/CardMedia';
import CardContent from '@material-ui/core/CardContent/CardContent';
import CardActions from '@material-ui/core/CardActions/CardActions';
import Button from '@material-ui/core/Button/Button';
import { deleteCall } from '../../services/restClient';
import { urls } from '../../constants/urls';

export default class ItemComponent extends Component {
  static propTypes = {
    description: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    removeItem: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.handleDelPictureClick = this.handleDelPictureClick.bind(this);
  }

  handleDelPictureClick() {
    const { id, removeItem } = this.props;

    deleteCall(`${urls.DELETE_IMAGE}${id}`)
      .then(() => removeItem(id))
      .catch(() => console.log(`error while deleting image`));
  }

  render() {
    const { classes } = this.props;
    const { description, image, title } = this.props;

    return(
      <div>
        <Card className={classes.card}>
          <CardActionArea>
            <CardMedia
              className={classes.media}
              image={image}
              title={title}
            />
            <CardContent>
              <Typography component="p">
                {description}
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions className={classes.actions}>
            <Button color="secondary" onClick={this.handleDelPictureClick}>
              Delete
            </Button>
          </CardActions>
        </Card>
      </div>
    );
  }
}