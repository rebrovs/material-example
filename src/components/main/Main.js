import { compose } from 'recompose';
import { withStyles } from '@material-ui/core/styles/index';

import MainComponent from './MainComponent';
import { withGetCall } from '../../hoc/withGetCall';
import { urls } from '../../constants/urls';

const styles = {
  root: {
    margin: 20
  },
  grid: {
    flexGrow: 1
  },
  fab: {
    position: 'fixed',
    bottom: 50,
    right: 50
  }
};

const enhance = compose(
  withStyles(styles),
  withGetCall(urls.LOAD_IMAGES)
);

const Main = enhance(MainComponent);

// export default withStyles(styles)(MainComponent);
export default Main;