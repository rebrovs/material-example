import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid/Grid';
import Fab from '@material-ui/core/Fab/Fab';
import AddIcon from '@material-ui/icons/Add';

import Item from '../item/Item';
import UploadModal from '../uploadmodal/UploadModal';

export default class MainComponent extends Component {
  static propTypes = {
    data: PropTypes.arrayOf(
      PropTypes.shape({
        description: PropTypes.string.isRequired,
        id: PropTypes.number.isRequired,
        image: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
      })
    ).isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      images: props.data,
      showLoginDialog: false
    };

    this.addItem = this.addItem.bind(this);
    this.removeItem = this.removeItem.bind(this);
    this.setUploadDialogVisibility = this.setUploadDialogVisibility.bind(this);
  }

  setUploadDialogVisibility(visibility) {
    if (this.state.showLoginDialog !== visibility) {
      this.setState({ showLoginDialog: visibility });
    }
  }

  addItem(item) {
    const { images } = this.state;

    images.push(item);

    this.setState({ images });
  }

  removeItem(id) {
    const { images } = this.state;
    const image = images.find((item) => item.id === id);
    const index = images.indexOf(image);

    images.splice(index, 1);

    this.setState({ images });
  }

  renderCards() {
    const { images } = this.state;
    const items = [];

    images.forEach((item) => {
      items.push(
        <Item
          description={item.description}
          id={item.id}
          image={item.image}
          key={item.id}
          title={item.title}
          removeItem={this.removeItem}
        />
      )
    });

    return items;
  }

  render() {
    const { classes } = this.props;
    const { showLoginDialog } = this.state;

    return(
      <div className={classes.root}>
        <Grid className={classes.grid} justify="center" container>
          {this.renderCards()}
        </Grid>

        <Fab className={classes.fab} color="primary" onClick={() => this.setUploadDialogVisibility(true)}>
          <AddIcon />
        </Fab>

        <UploadModal
          addItem={this.addItem}
          setVisibility={this.setUploadDialogVisibility}
          visible={showLoginDialog}
        />
      </div>
    );
  }
}