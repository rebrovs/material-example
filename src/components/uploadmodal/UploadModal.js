import { compose } from 'recompose';
import { withStyles } from '@material-ui/core/styles/index';
import UploadModalComponent from './UploadModalComponent';

const styles = {
  upload: {
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  input: {
    display: 'none'
  },
};

const enhance = compose(
  withStyles(styles)
);

const UploadModal = enhance(UploadModalComponent);

export default UploadModal;