import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import DialogActions from '@material-ui/core/DialogActions/DialogActions';
import Button from '@material-ui/core/Button/Button';
import TextField from '@material-ui/core/TextField/TextField';
import Typography from '@material-ui/core/Typography/Typography';

import { postCall } from '../../services/restClient';
import { urls } from '../../constants/urls';

export default class UploadModalComponent extends Component {
  static propTypes = {
    addItem: PropTypes.func.isRequired,
    setVisibility: PropTypes.func.isRequired,
    visible: PropTypes.bool
  };

  constructor(props) {
    super(props);

    this.state = {
      title: '',
      description: '',
      file: null,
      error: false
    };

    this.handleTitleInput = this.handleTitleInput.bind(this);
    this.handleDescriptionInput = this.handleDescriptionInput.bind(this);
    this.handleUploadInput = this.handleUploadInput.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
    this.handleUploadClick = this.handleUploadClick.bind(this);
    this.handleEnterKeyPress = this.handleEnterKeyPress.bind(this);
  }

  handleTitleInput(e) {
    this.setState({
      title: e.target.value
    });
  }

  handleDescriptionInput(e) {
    this.setState({
      description: e.target.value
    });
  }

  handleUploadInput(e) {
    this.setState({
      file: e.target.files[0]
    });
  }

  handleEnterKeyPress(e) {
    if (e.key === 'Enter') {
      this.handleUploadClick();
      e.preventDefault();
    }
  }

  handleDialogClose() {
    this.props.setVisibility(false);
    this.setState({
      title: '',
      description: '',
      file: null,
      error: false
    });
  }

  handleUploadClick() {
    const { addItem } = this.props;
    const { description, file, title } = this.state;
    const data = new FormData();

    data.set('description', description);
    data.set('file', file);
    data.set('title', title);

    postCall(urls.UPLOAD_IMAGE, data)
      .then((resp) => {
        const { data } = resp;

        addItem(data);

        this.setState({ error: false });
        this.handleDialogClose();
      })
      .catch((err) => {
        const { response: { data: { status } } } = err;

        if (status !== 200) {
          this.setState({ error: true });
        }

        return err;
      });
  }

  renderFileName() {
    const { classes } = this.props;
    const { file }  = this.state;

    if (file) {
      return (
        <Typography
          color="primary"
          className={classes.upload}
          variant="subheading"
        >
          {file.name}
        </Typography>
      );
    }
  }

  render() {
    const { classes, visible } = this.props;
    const { description, error, title }  = this.state;

    return (
      <Dialog
        open={visible}
        onClose={this.handleDialogClose}
      >
        <DialogTitle>Upload your image</DialogTitle>

        <DialogContent>
          <TextField
            error={error}
            id="Title"
            label="Title"
            margin="dense"
            type="text"
            value={title}
            autoFocus
            fullWidth
            required
            onChange={this.handleTitleInput}
            onKeyPress={this.handleEnterKeyPress}
          />
          <TextField
            error={error}
            id="Description"
            label="Description"
            margin="dense"
            type="text"
            value={description}
            fullWidth
            required
            onChange={this.handleDescriptionInput}
            onKeyPress={this.handleEnterKeyPress}
          />

          <input
            accept="image/*"
            className={classes.input}
            id="contained-button-file"
            multiple
            type="file"
            onChange={this.handleUploadInput}
          />
          <label htmlFor="contained-button-file">
            <Button
              className={classes.upload}
              color="primary"
              component="span"
              >
              Choose file to upload
            </Button>
          </label>

          {this.renderFileName()}
        </DialogContent>

        <DialogActions>
          <Button
            color="secondary"
            onClick={this.handleDialogClose}
          >
            Cancel
          </Button>
          <Button
            color="primary"
            type="submit"
            onClick={this.handleUploadClick}
          >
            Upload
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}