export const httpMethods = {
  GET: 'get',
  POST: 'post',
  DELETE: 'delete'
};
