const ADDRESS = 'localhost';
const PORT = '8080';

export const urls = {
  LOAD_IMAGES: `http://${ADDRESS}:${PORT}/rest/images`,
  UPLOAD_IMAGE: `http://${ADDRESS}:${PORT}/rest/upload`,
  DELETE_IMAGE: `http://${ADDRESS}:${PORT}/rest/delete/`
};