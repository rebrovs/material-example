import React, { Component } from 'react';
import { getCall } from '../services/restClient';
import Typography from '@material-ui/core/Typography/Typography';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';

export const withGetCall = (url) => {
  return (WrappedComponent) => {
    class WithGetCall extends Component {
      constructor(props) {
        super(props);

        this.state = {
          data: null,
          error: null,
          isLoading: true
        };
      }

      componentDidMount() {
        getCall(url)
          .then((response) => {
            const { data } = response;

            this.setState({
              data,
              isLoading: false
            });
          })
          .catch(({ response }) => {
            if (response) {
              const { data: { message } } = response;

              this.setState({
                error: message,
                isLoading: false
              });
            } else {
              this.setState({
                error: 'Server is not responding!',
                isLoading: false
              });
            }
          });
      }

      renderLoading() {
        return (
          <div align="center">
            <CircularProgress />
          </div>
        );
      }

      renderError() {
        return (
          <Typography
            align="center"
            color="error"
            variant="inherit"
          >
            Oops! Something went wrong. This page didn't load.
          </Typography>
        );
      }

      renderWrapper() {
        const { data } = this.state;

        return (
          <WrappedComponent
            {...this.props}
            data={data}
          />
        );
      }

      render() {
        const { error, isLoading } = this.state;

        return (
          <div>
            {!isLoading && !error && this.renderWrapper()}
            {isLoading && this.renderLoading()}
            {error && this.renderError()}
          </div>
        );
      }
    }

    return WithGetCall;
  }
};