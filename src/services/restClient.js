import axios from 'axios';
import { httpMethods } from '../constants/httpMethods';

export const getCall = (url) => {
  return axios({
    method: httpMethods.GET,
    url
  });
};

export const postCall = (url, data) => {
  return axios({
    method: httpMethods.POST,
    url,
    data
  });
};

export const deleteCall = (url) => {
  return axios({
    method: httpMethods.DELETE,
    url
  });
};